const passport = require('passport');
const LocalStrategy = require('passport-local/lib').Strategy;
const JwtStrategy = require('passport-jwt/lib').Strategy;
const { ExtractJwt } = require('passport-jwt/lib');
const User = require('../models/user');

// LocalStrategy initialization
passport.use('local', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
((username, password, done) => {
  User.findByCredentials(username, password, (error, user) => {
    if (error) return done(error, false);
    if (user) return done(null, user);
    return done(null, false);
  });
})));

// JwtStrategy initialization
const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET;

passport.use(new JwtStrategy(opts, ((jwt_payload, done) => {
  User.findOne({ _id: jwt_payload._id }, (err, user) => {
    if (err) {
      return done(err, false);
    }
    if (user) {
      return done(null, user);
    } 
    return done(null, false);
        
  });
})));

module.exports = passport;
