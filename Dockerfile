FROM node:10.15-alpine

WORKDIR /usr/src/app
COPY . .
RUN apk --no-cache add --virtual builds-deps build-base python
RUN apk add --no-cache openssl
RUN npm install

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

ARG NPM_SCRIPT=start
ENV NPM_SCRIPT ${NPM_SCRIPT}
CMD dockerize -wait tcp://rabbitmq:5672 -timeout 1m npm ${NPM_SCRIPT}
