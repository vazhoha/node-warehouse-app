const request = require('supertest');
const mongoose = require('mongoose');
const expect = require('chai').expect;
const should = require('chai').should();
const app = require('../app');
const Category = require('../models/category');
const {
  userOneToken,
  categoryOne,
  categoryOneId,
  categoryTwo,
  categoryTwoId,
  setupDatabase,
  clearDatabase
} = require('./fixtures/db');

beforeEach(setupDatabase);
after(clearDatabase);

function validateResponse(res, category) {
  expect(res.body).to.be.an('object');
  res.body._id.should.equal(category._id.toString());
  res.body.description.should.equal(category.description);
  res.body.amount.should.equal(category.amount);
}

describe('Tests for Category API Endpoints', () => {
  describe('GET /api/categories', () => {
    it('should fetch all categories', async () => {
      const res = await request(app)
        .get('/api/categories/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);

      expect(res.body).to.be.an('array').that.have.lengthOf(2);
      const first = await Category.findById(res.body[0]._id);
      const second = await Category.findById(res.body[1]._id);
      expect(first).not.to.be.null;
      expect(second).not.to.be.null;
    });

    it('should not fetch categories without user token', async () => {
      await request(app)
        .get('/api/categories/')
        .send()
        .expect(401);
    });

    it('should not fetch categories if user token is invalid', async () => {
      await request(app)
        .get('/api/categories/')
        .set('Authorization', `Bearer ${userOneToken.split('.').slice(1, 2)}`)
        .send()
        .expect(401);
    });

    it('should get category by valid id and token', async () => {
      const res = await request(app)
        .get(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);
      validateResponse(res, categoryTwo);
    });

    it('should not get category without user token', async () => {
      await request(app)
        .get(`/api/categories/${categoryTwoId}`)
        .send()
        .expect(401);
    });

    it('should not get category by invalid user token', async () => {
      await request(app)
        .get(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken.split('.')[1]}`)
        .send()
        .expect(401);
    });

    it('should not get category by invalid id', async () => {
      await request(app)
        .get(`/api/categories/${new mongoose.Types.ObjectId()}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(404);
    });
  });

  describe('POST /api/categories', () => {
    it('should create another new category', async () => {
      const res = await request(app)
        .post('/api/categories/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Детское питание',
          amount: 220
        })
        .expect(201);

      const category = await Category.findOne(res.body);
      expect(category).not.to.be.null;
    });

    it('should create new category using RabbitMQ', async () => {
      const categoryObj = {
        description: 'Детское питание',
        amount: 220
      };

      await request(app)
        .post('/api/delay/categories/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send(categoryObj)
        .expect(200);

      const category = await Category.findOne(categoryObj);
      expect(category).not.to.be.null;
    });

    it('should create new category without amount', async () => {
      const res = await request(app)
        .post('/api/categories/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Мучные изделия'
        })
        .expect(201);

      const category = await Category.findOne(res.body);
      expect(category).not.to.be.null;
      category.amount.should.equal(0);
    });

    it('should not create new category using RabbitMQ without user token', async () => {
      const categoryObj = {
        description: 'Детское питание',
        amount: 220
      };

      await request(app)
        .post('/api/delay/categories/')
        .send(categoryObj)
        .expect(401);

      const category = await Category.findOne(categoryObj);
      expect(category).to.be.null;
    });

    it('should not create new category using RabbitMQ with invalid user token', async () => {
      const categoryObj = {
        description: 'Детское питание',
        amount: 220
      };

      await request(app)
        .post('/api/delay/categories/')
        .set('Authorization', `Bearer ${userOneToken.split('.').slice(0, 1)}`)
        .send(categoryObj)
        .expect(401);

      const category = await Category.findOne(categoryObj);
      expect(category).to.be.null;
    });

    it('should not create new category without description', async () => {
      const res = await request(app)
        .post('/api/categories/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          amount: 220
        })
        .expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should not create another category without user token', async () => {
      await request(app)
        .post('/api/categories/')
        .send({
          description: 'Детское питание',
          amount: 220
        }).expect(401);
    });

    it('should not create category for user which having invalid token', async () => {
      await request(app)
        .post('/api/categories/')
        .set('Authorization', `Bearer ${userOneToken.split('.').slice(0, 1)}`)
        .send({
          description: 'Детское питание',
          amount: 220
        })
        .expect(401);
    });

    it('should not create category if amount is less than zero', async () => {
      await request(app)
        .post('/api/categories/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Детское питание',
          amount: -1
        })
        .expect(400);
    });
  });

  describe('PATCH /api/categories', () => {
    it('should update second category by valid id', async () => {
      const res = await request(app)
        .patch(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Детское питание и одежда',
          amount: 45
        })
        .expect(200);

      const category = await Category.findById(categoryTwoId);
      category.description.should.equal('Детское питание и одежда');
      category.amount.should.equal(45);

      validateResponse(res, category);
    });

    it('should update only amount property in second category using valid id and token', async () => {
      const res = await request(app)
        .patch(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          amount: 60
        })
        .expect(200);

      const category = await Category.findById(categoryTwoId);
      category.amount.should.equal(60);
      validateResponse(res, category);
    });

    it('should update only description property in second category using valid id and token', async () => {
      const res = await request(app)
        .patch(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Детское питание и игрушки'
        })
        .expect(200);

      const category = await Category.findById(categoryTwoId);
      category.description.should.equal('Детское питание и игрушки');
      category.amount.should.equal(categoryTwo.amount);

      validateResponse(res, category);
    });

    it('should not update description property in second category using invalid id', async () => {
      await request(app)
        .patch(`/api/categories/${new mongoose.Types.ObjectId()}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Морепродукты'
        })
        .expect(404);

      const category = await Category.findById(categoryTwoId);
      category.description.should.equal(categoryTwo.description);
      category.amount.should.equal(categoryTwo.amount);
    });

    it('should not update second category using invalid user token', async () => {
      await request(app)
        .patch(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken.toUpperCase()}`)
        .send({
          description: 'Морепродукты',
          amount: 110
        })
        .expect(401);

      const category = await Category.findById(categoryTwoId);
      category.description.should.equal(categoryTwo.description);
      category.amount.should.equal(categoryTwo.amount);
    });

    it('should not update _id property in second category', async () => {
      await request(app)
        .patch(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          _id: new mongoose.Types.ObjectId()
        })
        .expect(400);
      const category = await Category.findById(categoryTwoId);
      category.description.should.equal(categoryTwo.description);
      category.amount.should.equal(categoryTwo.amount);
    });
  });

  describe('DELETE /api/categories', () => {

    it('should delete only first category', async () => {
      const res = await request(app)
        .delete(`/api/categories/${categoryOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);

      categoryOne.description.should.equal(res.body.description);
      categoryOne.amount.should.equal(res.body.amount);

      const categories = await Category.find({});
      expect(categories).to.be.an('array').that.have.lengthOf(1);
      categories[0].description.should.equal(categoryTwo.description);
    });

    it('should not delete category by invalid id', async () => {
      await request(app)
        .delete(`/api/categories/${new mongoose.Types.ObjectId()}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(404);

      const categories = await Category.find({});
      expect(categories).to.be.an('array').that.have.lengthOf(2);
    });

    it('should not delete category by invalid token', async () => {
      await request(app)
        .delete(`/api/categories/${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken.toUpperCase()}`)
        .send()
        .expect(401);

      const secondCategory = await Category.findById(categoryTwoId);
      should.exist(secondCategory);
    });
  });
});
