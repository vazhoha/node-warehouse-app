const request = require('supertest');
const mongoose = require('mongoose');
const expect = require('chai').expect;
const should = require('chai').should();
const jwt = require('jsonwebtoken');
const app = require('../app');
const User = require('../models/user');
const {
  userOneId,
  userOneToken,
  userOne,
  non_existingUser,
  clearDatabase,
  setupDatabase,
} = require('./fixtures/db');

function checkUserToken(receivedToken, objectId) {
  const decoded = jwt.verify(receivedToken, process.env.JWT_SECRET);
  decoded._id.should.equal(objectId.toString());
}

function checkUserProperties(user, credentials, checkPassword) {
  expect(user).not.to.be.null;
  user.name.should.equal(credentials.name);
  user.email.should.equal(credentials.email);
  user.age.should.equal(credentials.age ? credentials.age : 0);
  if (checkPassword) expect(user.password).to.be.undefined;
}

const anotherUserCredentials = {
  name: 'Vladislav Vazhoha',
  email: 'vazhoha@example.com',
  password: '12345678'
};

beforeEach(setupDatabase);
after(clearDatabase);

describe('Tests for User API Endpoints', () => {
  describe('POST /api/users/', () => {
    it('should fetch profile for user', async () => {
      const res = await request(app)
        .post('/api/users/me')
        .send({
          email: userOne.email,
          password: userOne.password
        }).expect(200);

      checkUserProperties(res.body, userOne, true);
    });

    it('should not fetch profile for non-existing user', async () => {
      await request(app)
        .post('/api/users/me')
        .send({
          email: non_existingUser.email,
          password: non_existingUser.password
        }).expect(500);
    });

    it('should not fetch profile for user without password', async () => {
      await request(app)
        .post('/api/users/me')
        .send({
          email: userOne.email,
        }).expect(400);
    });

    it('should not fetch profile for user without email', async () => {
      await request(app)
        .post('/api/users/me')
        .send({
          password: userOne.password,
        }).expect(400);
    });

    it('should not sign up a new user without name', async () => {
      const user = { ...anotherUserCredentials };
      delete user.name;
      const res = await request(app).post('/api/users').send({
        ...user
      }).expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should not sign up a new user without email', async () => {
      const user = { ...anotherUserCredentials };
      delete user.email;
      const res = await request(app).post('/api/users').send({
        ...user
      }).expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should not sign up a new user without password', async () => {
      const user = { ...anotherUserCredentials };
      delete user.password;
      const res = await request(app).post('/api/users').send({
        ...user
      }).expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should not sign up a new user without credentials', async () => {
      const res = await request(app).post('/api/users')
        .send()
        .expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should sign up a new user', async () => {
      const res = await request(app).post('/api/users').send({
        ...anotherUserCredentials
      }).expect(201);

      // Assert that the database was changed correctly
      const userId = res.body.user._id;
      const user = await User.findById(userId);
      checkUserProperties(user, anotherUserCredentials);
      checkUserToken(res.body.token, user._id);
    });

    it('should not sign up existing user', async () => {
      const res = await request(app).post('/api/users')
        .send({
          ...userOne
        }).expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should login existing user', async () => {
      const res = await request(app).post('/api/users/login')
        .send({
          email: userOne.email,
          password: userOne.password
        }).expect(200);
      const user = res.body.user;
      const token = res.body.token;
      checkUserProperties(user, userOne);
      checkUserToken(token, user._id);
    });

    it('should not login non-existing user', async () => {
      await request(app).post('/api/users/login')
        .send({
          email: non_existingUser.email,
          password: non_existingUser.password
        }).expect(500);
    });

    it('should not login user without credentials', async () => {
      await request(app).post('/api/users/login')
        .send()
        .expect(400);
    });
  });

  describe('PATCH /api/users/', () => {
    it('should update valid user fields', async () => {
      const updatedCredentials = {
        name: 'Vlad',
        email: 'vladislav.va@room4.team'
      };

      const res = await request(app)
        .patch('/api/users/me')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send(updatedCredentials)
        .expect(200);

      checkUserProperties(res.body, {
        name: updatedCredentials.name,
        password: userOne.password,
        email: updatedCredentials.email
      });
    });

    it('should not update invalid user fields', async () => {
      const updatedCredentials = {
        _id: new mongoose.Types.ObjectId(),
        name: 'Vlad',
        email: 'vladislav.va@room4.team',
        age: 19
      };

      await request(app)
        .patch('/api/users/me')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send(updatedCredentials)
        .expect(400);
    });

    it('should not update user with invalid token', async () => {
      const updatedCredentials = {
        _id: new mongoose.Types.ObjectId(),
        name: 'Vlad',
        email: 'vladislav.va@room4.team',
        age: 19
      };

      await request(app)
        .patch('/api/users/me')
        .set('Authorization', 'Bearer 123')
        .send(updatedCredentials)
        .expect(401);
    });
  });

  describe('DELETE /api/users/', () => {
    it('should not delete account for unauthenticated user', async () => {
      await request(app)
        .delete('/api/users/me')
        .send()
        .expect(401);
    });

    it('should not delete account for user which having invalid token', async () => {
      await request(app)
        .delete('/api/users/me')
        .set('Authorization', 'Bearer 123')
        .send()
        .expect(401);
    });

    it('should delete account for valid user', async () => {
      await request(app)
        .delete('/api/users/me')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);

      // Assert that the database was changed correctly
      const user = await User.findById(userOneId);
      expect(user).to.be.null;
    });
  });
});
