const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../app');
const expect = require('chai').expect;
const should = require('chai').should();
const Product = require('../models/product');
const {
  userOneToken,
  categoryOneId,
  categoryTwoId,
  productOne,
  productOneId,
  productTwo,
  productThree,
  setupDatabase,
  clearDatabase
} = require('./fixtures/db');

beforeEach(setupDatabase);
after(clearDatabase);

function validateResponse(res, product) {
  expect(product).not.to.be.null;
  expect(res.body).to.be.an('object');
  res.body._id.should.equal(product._id.toString());
  if (typeof product.category === 'string') res.body.category.should.equal(product.category);
  else res.body.category.should.equal(product.category._id.toString());
  res.body.description.should.equal(product.description);
  res.body.amount.should.equal(product.amount)
}

describe('Tests for Product API Endpoints', function () {
  describe('GET /api/products', function () {
    it('should fetch all products', async () => {
      const res = await request(app)
        .get('/api/products/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);
      expect(res.body).to.be.an('array').that.have.lengthOf(3);
    });

    it('should fetch all products in second category', async () => {
      const res = await request(app)
        .get(`/api/products/?category=${categoryTwoId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);
      expect(res.body).to.be.an('array').that.have.lengthOf(2);
      res.body[0].description.should.equal(productTwo.description);
      res.body[1].description.should.equal(productThree.description);
    });

    it('should not fetch products in non-existing category', async () => {
      const res = await request(app)
        .get(`/api/products/?category=${new mongoose.Types.ObjectId()}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);
      expect(res.body).to.be.an('array').that.have.lengthOf(0);
    });

    it('should not fetch products without user token', async () => {
      await request(app)
        .get('/api/products/')
        .send()
        .expect(401);
    });

    it('should not fetch products if user token is invalid', async () => {
      await request(app)
        .get('/api/products/')
        .set('Authorization', `Bearer ${userOneToken.split('.').slice(1, 2)}`)
        .send()
        .expect(401);
    });

    it('should get product by valid id and token', async () => {
      const res = await request(app)
        .get(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);

      validateResponse(res, productOne);
    });

    it('should not get product without user token', async () => {
      await request(app)
        .get(`/api/products/${productOneId}`)
        .send()
        .expect(401);
    });

    it('should not get product by invalid user token', async () => {
      await request(app)
        .get(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken.split('.')[1]}`)
        .send()
        .expect(401);
    });

    it('should not get product by invalid id', async () => {
      await request(app)
        .get(`/api/products/${new mongoose.Types.ObjectId()}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(404);
    });
  });

  describe('POST /api/products', function () {
    it('should create new product', async () => {
      const res = await request(app)
        .post('/api/products/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          category: categoryOneId,
          description: 'Сосиски',
          amount: 43
        }).expect(201);

      const product = await Product.findById(res.body._id);
      validateResponse(res, product);
    });

    it('should create new product without amount', async () => {
      const res = await request(app)
        .post('/api/products/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          category: categoryOneId,
          description: 'Котлеты',
        }).expect(201);

      const product = await Product.findOne(res.body);
      product.amount.should.equal(0);
      validateResponse(res, product);
    });

    it('should not create new product without category', async () => {
      const res = await request(app)
        .post('/api/products/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Макароны',
          amount: 12
        }).expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should not create new product without description', async () => {
      const res = await request(app)
        .post('/api/products/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          category: categoryOneId,
          amount: 12
        }).expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should not create new product if amount is less than zero', async () => {
      const res = await request(app)
        .post('/api/products/')
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          category: categoryOneId,
          description: 'Макароны',
          amount: -1
        }).expect(400);
      expect(res.errmsg).not.to.be.null;
    });

    it('should not create new product using invalid user token', async () => {
      const res = await request(app)
        .post('/api/products/')
        .set('Authorization', `Bearer ${userOneToken.split('.').slice(1, 2)}`)
        .send({
          category: categoryOneId,
          description: 'Макароны',
          amount: 123
        }).expect(401);
      expect(res.errmsg).not.to.be.null;
    });
  });

  describe('PATCH /api/products', function () {
    it('should update product by valid id', async () => {
      const res = await request(app)
        .patch(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: "Блины",
          amount: 12
        })
        .expect(200);
      const product = await Product.findById(productOneId);

      product.description.should.equal('Блины');
      product.amount.should.equal(12);

      validateResponse(res, product);
    });

    it('should update amount property in product using valid id and token', async () => {
      const res = await request(app)
        .patch(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          amount: 29
        })
        .expect(200);
      const product = await Product.findById(productOneId);
      product.amount.should.equal(29);
      validateResponse(res, product);
    });

    it('should update only description property in product using valid id and token', async () => {
      const res = await request(app)
        .patch(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Булочки'
        })
        .expect(200);
      const product = await Product.findById(productOneId);

      product.description.should.equal('Булочки');
      product.amount.should.equal(productOne.amount);

      validateResponse(res, product);
    });

    it('should not update description property in product using using invalid id', async () => {
      await request(app)
        .patch(`/api/products/${new mongoose.Types.ObjectId()}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          description: 'Лаваш'
        })
        .expect(404);

      const product = await Product.findById(productOneId);
      product.description.should.equal(productOne.description);
      product.amount.should.equal(productOne.amount);
    });

    it('should not update category property in product using invalid user token', async () => {
      await request(app)
        .patch(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer 12323435`)
        .send({
          category: 'test',
        })
        .expect(401);

      const product = await Product.findById(productOneId);
      product.category.toString().should.equal(categoryOneId.toString());
      product.description.should.equal(productOne.description);
      product.amount.should.equal(productOne.amount);
    });

    it('should not update category property in product using invalid ObjectID', async () => {
      await request(app)
        .patch(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          category: 'test',
        })
        .expect(400);

      const product = await Product.findById(productOneId);
      product.category.toString().should.equal(categoryOneId.toString());
      product.description.should.equal(productOne.description);
      product.amount.should.equal(productOne.amount);
    });

    it('should not update _id property in product', async () => {
      await request(app)
        .patch(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send({
          _id : new mongoose.Types.ObjectId(),
          amount: 1553
        })
        .expect(400);

      const product = await Product.findById(productOneId);
      product.description.should.equal(productOne.description);
      product.amount.should.equal(productOne.amount);
    });
  });

  describe('DELETE /api/products', function () {

    it('should delete only first product', async () => {
      const res = await request(app)
        .delete(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(200);

      productOne.description.should.equal(res.body.description);
      productOne.amount.should.equal(res.body.amount);

      const products = await Product.find({});
      expect(products).to.be.an('array').that.have.lengthOf(2);
      products[0].description.should.equal(productTwo.description);
    });

    it('should not delete product by invalid id', async () => {
      await request(app)
        .delete(`/api/products/${new mongoose.Types.ObjectId()}`)
        .set('Authorization', `Bearer ${userOneToken}`)
        .send()
        .expect(404);

      const products = await Product.find({});
      expect(products).to.be.an('array').that.have.lengthOf(3);
    });

    it('should not delete product by invalid user token', async () => {
      await request(app)
        .delete(`/api/products/${productOneId}`)
        .set('Authorization', `Bearer ${userOneToken.toUpperCase()}`)
        .send()
        .expect(401);

      const products = await Product.find({});
      expect(products).to.be.an('array').that.have.lengthOf(3);
    });
  });
});
