const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = require('../../models/user');
const Category = require('../../models/category');
const Product = require('../../models/product');

// Users for tests
const userOneId = new mongoose.Types.ObjectId();
const userOneToken = jwt.sign({ _id: userOneId }, process.env.JWT_SECRET);
const userOne = {
  _id: userOneId,
  name: 'Nikita',
  email: 'nikita@example.com',
  password: '12345678'
};

const non_existingUser = { ...userOne };
non_existingUser.password += '9';

// Categories for tests
const categoryOneId = new mongoose.Types.ObjectId();
const categoryOne = {
  _id: categoryOneId,
  description: 'Мясные изделия',
  amount: 74
};

const categoryTwoId = new mongoose.Types.ObjectId();
const categoryTwo = {
  _id: categoryTwoId,
  description: 'Овощи и фрукты',
  amount: 51
};

// Products for tests
const productOneId = new mongoose.Types.ObjectId();
const productOne = {
  _id: productOneId,
  category: categoryOneId,
  description: 'Колбаса',
  amount: 24
};

const productTwo = {
  category: categoryTwoId,
  description: 'Огурцы',
  amount: 43
};

const productThree = {
  category: categoryTwoId,
  description: 'Помидоры',
  amount: 36
};

const clearDatabase = async () => {
  await User.deleteMany({});
  await Category.deleteMany({});
  await Product.deleteMany({});
};

const setupDatabase = async () => {
  await clearDatabase();
  await new User(userOne).save();
  await new Category(categoryOne).save();
  await new Category(categoryTwo).save();
  await new Product(productOne).save();
  await new Product(productTwo).save();
  await new Product(productThree).save();
};

module.exports = {
  userOneId,
  userOneToken,
  userOne,
  non_existingUser,
  categoryOne,
  categoryOneId,
  categoryTwo,
  categoryTwoId,
  productOne,
  productOneId,
  productTwo,
  productThree,
  clearDatabase,
  setupDatabase
};
