const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('./middleware/passport');
require('./db/mongoose');

// Load consumers
require('./consumers/categories');
require('./consumers/logger');

const categoriesRouter = require('./routes/categories');
const productsRouter = require('./routes/products');

const apiCategoriesRouter = require('./routes/api/categories');
const apiProductsRouter = require('./routes/api/products');
const apiUsersRouter = require('./routes/api/users');
const apiDelayRouter = require('./routes/api/delay');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

// Root endpoint
app.get('/', async (req, res) => {
  res.render('index.html');
});

app.post('/users/login', passport.authenticate('local', { session: false }), async (req, res) => {
  res.redirect('/categories');
});

// API endpoints
app.use('/api/users', apiUsersRouter);
app.use('/api/categories', apiCategoriesRouter);
app.use('/api/products', apiProductsRouter);
app.use('/api/users', apiUsersRouter);
app.use('/api/delay', apiDelayRouter);

// HTML endpoints
app.use('/categories', categoriesRouter);
app.use('/products', productsRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  if (!req.originalUrl.includes('api')) res.render('error');
  else res.send(err);
});

module.exports = app;
