const amqp = require('amqplib');
const chalk = require('chalk');

// Logger consumer
(async () => {
  try {
    const conn = await amqp.connect(process.env.AMQP_URL);
    process.once('SIGINT', () => {
      conn.close();
    });

    const ch = await conn.createChannel();
    await ch.assertExchange('topic_categories', 'topic', { durable: false });

    const { queue } = await ch.assertQueue('', { exclusive: true });
    await ch.bindQueue(queue, 'topic_categories', '#.log'); // or using '#' pattern

    await ch.consume(queue, (msg) => {
      console.log(chalk.yellow('[Logger]'), `Received ${msg.fields.routingKey} :`, msg.content.toString());
    }, { noAck: true });
  } catch (e) {
    console.error(e);
  }
})();
