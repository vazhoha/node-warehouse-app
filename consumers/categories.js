const amqp = require('amqplib');
const Category = require('../models/category');

// Categories consumer
(async () => {
  try {
    const conn = await amqp.connect(process.env.AMQP_URL);
    process.once('SIGINT', () => {
      conn.close();
    });

    const ch = await conn.createChannel();
    await ch.assertExchange('topic_categories', 'topic', { durable: false });

    const { queue } = await ch.assertQueue('', { exclusive: true });
    await ch.bindQueue(queue, 'topic_categories', 'categories.*');

    await ch.consume(queue, async (msg) => {
      const body = JSON.parse(msg.content);
      const category = new Category(body);
      await category.save();
    }, { noAck: true });
  } catch (e) {
    console.error(e);
  }
})();
