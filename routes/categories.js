const express = require('express');
const router = express.Router();
const moment = require('moment');
const Category = require('../models/category');

const title = 'Categories';

// GET Categories
router.get('/', async (req, res) => {
  try {
    const categories = await Category.find({});

    res.render('categories', {
      title,
      page: title,
      description: 'Выберите категорию',
      categories,
      moment
    });
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) return res.sendStatus(404);
    await category.populate('products').execPopulate();
    const title = 'Products';
    res.render('products', {
      title,
      page: title,
      description: category.description,
      products: category.products ? category.products : [],
      moment
    });
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
