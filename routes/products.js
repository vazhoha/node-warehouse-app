const express = require('express');
const router = express.Router();
const moment = require('moment');
const Product = require('../models/product');

const title = 'Products';

// GET Products
// /products/?category=id
router.get('/', async (req, res) => {
  try {
    const { category } = req.query;
    let products = await Product.find(category ? { category } : {});
    if (!products.length > 0) products = [];
    res.render('products', {
      title,
      page: title,
      description: 'Список всех продуктов',
      products,
      moment
    });
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
