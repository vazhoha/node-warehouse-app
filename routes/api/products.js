const express = require('express');
const router = express.Router();
const passport = require('../../middleware/passport');
const Product = require('../../models/product');

// GET Products
// /api/products/?category=id

router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { category } = req.query;
    let products = await Product.find(category ? { category } : {});
    if (products.length === 0) products = [];
    res.send(products);
  } catch (e) {
    res.status(500).send(e);
  }
});

// GET Product
router.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    if (!product) return res.sendStatus(404);
    res.send(product);
  } catch (e) {
    res.sendStatus(500);
  }
});

// POST Product
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const product = new Product(req.body);
    await product.save();
    res.status(201).send(product);
  } catch (e) {
    res.status(400).send(e);
  }
});

// PATCH Product
router.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['description', 'amount', 'expiration', 'category'];
    const isValidOperation = updates.every(update => allowedUpdates.includes(update));

    if (!isValidOperation) return res.status(400).send({ error: 'Invalid updates!' });

    const product = await Product.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true });
    if (!product) return res.sendStatus(404);
    res.send(product);
  } catch (e) {
    res.status(400).send(e);
  }
});

// DELETE Product
router.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    if (!product) return res.sendStatus(404);
    await product.remove();
    res.send(product);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
