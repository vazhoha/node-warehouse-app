const express = require('express');
const router = express.Router();
const amqp = require('amqplib');
const chalk = require('chalk');
const passport = require('../../middleware/passport');

// Publisher in POST Category
router.post('/categories', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const conn = await amqp.connect(process.env.AMQP_URL);
    const ch = await conn.createChannel();

    await ch.assertExchange('topic_categories', 'topic', { durable: false });
    ch.publish(
      'topic_categories',
      'categories.log',
      Buffer.from(JSON.stringify(req.body))
    );

    console.log(chalk.magenta('[Publisher]'), 'Sent categories.log :', req.body);

    await ch.close();
    await conn.close();
    res.send();
  } catch ({ message, stack }) {
    res.status(400).send({ message, stack });
  }
});

module.exports = router;
