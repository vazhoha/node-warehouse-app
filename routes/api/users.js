const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const passport = require('../../middleware/passport');

/* GET users listing. */

router.post('/me', passport.authenticate('local', { session: false }), async (req, res) => res.send(req.user));

router.post('/login', passport.authenticate('local', { session: false }), async (req, res) => {
  try {
    const token = await req.user.generateAuthToken();
    res.send({ user: req.user, token });
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post('/', async (req, res) => {
  try {
    const user = new User(req.body);
    await user.save();
    const token = await user.generateAuthToken();
    res.status(201).send({ user, token });
  } catch (e) {
    res.status(400).send(e);
  }
});

router.patch('/me', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password', 'age'];
    const isValidOperation = updates.every(update => allowedUpdates.includes(update));

    if (!isValidOperation) return res.status(400).send({ error: 'Invalid updates!' });

    // const user = await User.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true});

    updates.forEach(update => req.user[update] = req.body[update]);
    await req.user.save();

    res.send(req.user);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/me', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    await req.user.remove();
    res.send(req.user);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
