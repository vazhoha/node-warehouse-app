const express = require('express');
const router = express.Router();
const passport = require('../../middleware/passport');
const Category = require('../../models/category');

// GET Category
router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const categories = await Category.find({});
    res.send(categories);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) return res.sendStatus(404);
    res.send(category);
  } catch (e) {
    res.sendStatus(500);
  }
});

// POST Category
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const category = new Category(req.body);
    await category.save();
    res.status(201).send(category);
  } catch (e) {
    res.status(400).send(e);
  }
});

// PATCH Category
router.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['description', 'amount', 'expiration'];
    const isValidOperation = updates.every(update => allowedUpdates.includes(update));

    if (!isValidOperation) return res.status(400).send({ error: 'Invalid updates!' });

    const category = await Category.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true });
    if (!category) return res.sendStatus(404);
    res.send(category);
  } catch (e) {
    res.status(400).send(e);
  }
});

// DELETE Category
router.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) return res.sendStatus(404);
    await category.remove();
    res.send(category);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
