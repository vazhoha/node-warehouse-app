const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) throw new Error('Email is invalid!');
    }
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 7,
    validate(value) {
      if (value.toLowerCase().includes('password')) throw new Error('Password contain "password"');
    }
  },
  age: {
    type: Number,
    default: 0,
    validate(value) {
      if (value < 0) throw new Error('Age must be a positive number');
    }
  }
});

userSchema.methods.generateAuthToken = async function () {
  const user = this;
  return jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET, { expiresIn: '3m' });
};

userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;

  return userObject;
};

userSchema.statics.findByCredentials = (email, password, cb) => {
  User.findOne({ email }, (error, user) => {
    if (!user) return cb('Unable to login!', null);
    bcrypt.compare(password, user.password, (error, res) => {
      if (!res) return cb('Wrong password!!', null);
      cb(null, user);
    });
  });
};

// Hash the plain text password before saving
userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

const User = new mongoose.model('User', userSchema);

module.exports = User;
