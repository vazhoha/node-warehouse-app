const mongoose = require('mongoose');
const Product = require('../models/product');

const categorySchema = mongoose.Schema({
  description: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  amount: {
    type: Number,
    default: 0,
    min: 0,
  }
}, {
  timestamps: true
});

categorySchema.virtual('products', {
  ref: 'Product',
  localField: '_id',
  foreignField: 'category'
});

// Delete products when category is removed
categorySchema.pre('remove', async function (next) {
  const category = this;
  await Product.deleteMany({ category: category._id });
  next();
});

const Category = new mongoose.model('Category', categorySchema);

module.exports = Category;
