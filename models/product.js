const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
  description: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  amount: {
    type: Number,
    default: 0,
    min: 0
  },
  expiration: {
    type: Date,
    default: new Date().getTime()
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Category'
  }
}, {
  timestamps: true
});

const Product = new mongoose.model('Product', productSchema);

module.exports = Product;
